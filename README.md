# STATISTIKA

## VOSPlzeň, Základy programování, 1. semestr
---
* konzolová aplikace, jejímž úkolem je zobrazit u náhodně vylosovaných vět statistiku - počet samohlásek, počet souhlásek, počet slov nebo počet znaků ve větě celkem.